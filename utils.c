#include <string.h>
#include <stdlib.h>

void append_str(char **first, char *second)
{
    size_t len_first = strlen(*first);
    size_t len_second = strlen(second);

    *first = realloc(*first, len_first + len_second + 1);
    memcpy(&(*first)[len_first], second, len_second + 1);
}
