#include <string.h>
#include <stdlib.h>

#include <discord.h>

#include "utils.h"

void on_members(struct discord *client, const struct discord_message *msg)
{
    if (msg->author->bot)
        return; // Ignore bots

    struct discord_guild_member **members;
    struct discord_list_guild_members_params params = {
        .limit = 1000
    };

    ORCAcode status = discord_list_guild_members(
        client,
        msg->guild_id,
        &params,
        &members
    );

    if (status != ORCA_OK) {
        log_error("Cannot fetch the member list");
        struct discord_create_message_params params = {
            .content = "An error occured while fetching the member list",
            .message_reference = (struct discord_message_reference *) msg
        };
        discord_create_message(client, msg->channel_id, &params, NULL);
        return;
    }

    char *end_msg = strdup("```\n");
    for (uint_fast16_t i = 0; members[i]; i++) {
        append_str(&end_msg, members[i]->user->username);
        append_str(&end_msg, "#");
        append_str(&end_msg, members[i]->user->discriminator);
        append_str(&end_msg, "\n");
    }
    append_str(&end_msg, "```");
    struct discord_create_message_params end_params = {
        .content = end_msg,
        .message_reference = (struct discord_message_reference *) msg
    };
    discord_create_message(client, msg->channel_id, &end_params, NULL);
    free(end_msg);
}
