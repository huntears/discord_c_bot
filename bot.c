#include <discord.h>

#include "my_bot.h"

void on_ready(struct discord *client)
{
    const struct discord_user *bot = discord_get_self(client);

    log_info("badbot succesfully connected to Discord as %s#%s!",
        bot->username, bot->discriminator);
}

void on_ping(struct discord *client, const struct discord_message *msg)
{
    if (msg->author->bot)
        return; // Ignore bots

    struct discord_create_message_params params = {
        .content = "pong",
        .message_reference = (struct discord_message_reference *) msg
    };
    discord_create_message(client, msg->channel_id, &params, NULL);
}

int main(void)
{
    struct discord *client = discord_config_init("config.json");
    discord_set_on_ready(client, on_ready);
    discord_set_on_command(client, "ping", on_ping);
    discord_set_on_command(client, "members", on_members);

    discord_run(client);
    discord_cleanup(client);
    return 0;
}
