TARGET	:=	bot

SRC	:=	bot.c			\
		on_members.c	\
		utils.c			\

OBJ	:=	$(SRC:.c=.o)

CPPFLAGS	:=	-I/usr/local/include/orca/ -D_POSIX_C_SOURCE=200809L

CFLAGS	:=	-Wall -Wextra -std=c17 -O2

LDFLAGS	:=	-pthread -ldiscord -lcurl

CC	:=	clang

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $(OBJ) -o $(TARGET) $(LDFLAGS)

clean:
	rm -f *.o

fclean: clean
	rm -f $(TARGET)

re: fclean
	$(MAKE) all
